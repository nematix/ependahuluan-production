# EPendahuluan Production Node #

EPendahuluan Production Node repository for Docker deployment.

## Running an EPendahuluan Production Node

We have to make sure Docker & Fig is install on the production node. If everything is in place, then
we can proceed by cloning the EPendahuluan Production Node repository using following command.

    git clone https://bitbucket.org/marafoundry/ependahuluan-production.git ependahuluan
    cd ependahuluan/
    git submodule init
    git submodule update

 Start the containers.

    
    docker-compose -p ep up

We also can start containers in background by using following command

    docker-compose -p ep up -d

We can check the container is running using the following command

    docker-compose -p ep ps

Check logs of the container using below command

    docker-compose -p ep logs
