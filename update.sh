#!/bin/bash

echo "Resetting repos.."
cd www/vendor/doctrine/dbal/
git reset HEAD --hard
cd -

cd www
git reset HEAD --hard
cd -

echo "Updating repos.."
git pull origin master
git submodule update --remote

echo "Updating package.."
docker exec ependahuluan_web_1 composer update --no-scripts
docker exec ependahuluan_web_1 php artisan dump-autoload

echo "Clearing cache.."
docker exec ependahuluan_web_1 php artisan cache:clear


echo "Update complete!"
